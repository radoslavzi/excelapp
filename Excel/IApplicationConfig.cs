﻿using System;

namespace Excel
{
    public interface IApplicationConfig
    {
        void TransformAllFiles();
    }
}
