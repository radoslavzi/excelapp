﻿using System;
using Microsoft.Office.Interop.Excel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Excel
{
    public class ExcelApplication : IApplicationConfig, IDisposable
    {
        private Application _excel;
        private string _directoryPath;
        private Workbook _workBook;
        private DateTime _datetimeTest;
        private string[] _fileEntries;

        public ExcelApplication()
        {
            CultureInfo.CurrentCulture = new CultureInfo("de-DE"); ;
            this._excel = new Application();
            this._directoryPath = ConfigurationManager.AppSettings["filePath"];

            this._fileEntries = Directory.GetFiles(this._directoryPath, "*.*").Where(file => file.EndsWith(".xls") || file.EndsWith(".xlsx") || file.EndsWith(".xlsm")).ToArray();
            this._datetimeTest = DateTime.Now;
        }

        public void TransformAllFiles()
        {
            StringBuilder builder = new StringBuilder();

            if (this._fileEntries.Length == 0)
            {
                Console.WriteLine("No excel file in specified directory or wrong directory pointed!");
                return;
            }

            for (int i = 0; i < this._fileEntries.Length; i++)
            {
                this._workBook = this._excel.Workbooks.Open(this._fileEntries[i]);
                Worksheet sheet = (Worksheet)this._workBook.Worksheets[1];
                sheet.Activate();

                int index = this._workBook.FullName.LastIndexOf('.');
                string filePath = this._workBook.FullName.Substring(0, index) + ".csv";
                FileStream stream = File.Create(filePath);

                WriteDataToCSVFile(builder, sheet, stream);

                builder.Clear();
                this._workBook.Close(false);
            }

            Console.WriteLine();
            Console.WriteLine("----------------SUCCESS-------------");
        }

        private void WriteDataToCSVFile(StringBuilder builder, Worksheet sheet, FileStream stream)
        {
            using (StreamWriter writer = new StreamWriter(stream, Encoding.UTF8))
            {
                int colCount = sheet.UsedRange.Columns.Count;
                int rowCount = sheet.UsedRange.Rows.Count;
                bool isNewLine = false;
                for (int row = 1; row <= rowCount; row++)
                {
                    bool writtenSmth = false;
                    for (int col = 1; col <= colCount; col++)
                    {
                        string firstColumnValue = Convert.ToString((sheet.Cells[row, 1] as Range).Text);
                        if (string.IsNullOrEmpty(firstColumnValue))
                        {
                            break;
                        }

                        string updatedCellValue = this.GetCellUpdatedContent(sheet, row, col);

                        if (isNewLine)
                        {
                            isNewLine = false;
                            builder.AppendLine();
                        }

                        builder.Append(updatedCellValue);
                        builder.Append('|');
                        writtenSmth = true;
                    }

                    isNewLine = writtenSmth;
                }

                writer.Write(builder.ToString());
            }
        }

        private string GetCellUpdatedContent(Worksheet sheet, int row, int col)
        {
            string cellValue = Convert.ToString((sheet.Cells[row, col] as Range).Text);

            if (!string.IsNullOrEmpty(cellValue) && !string.IsNullOrWhiteSpace(cellValue) && !(sheet.Cells[row, col] as Range).HasFormula &&
                !DateTime.TryParse(cellValue, out this._datetimeTest))
            {
                string cell = cellValue.Replace('\n', ' ').Replace('|', ' ');
                sheet.Cells[row, col] = cell;

                Console.WriteLine(string.Format("cell[{0}, {1}] = {2}", row, col, cell));
                return cell;
            }

            return cellValue;
        }

        public void Dispose()
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(this._excel);
        }
    }
}