﻿using System;
using System.Diagnostics;
using System.Threading;

namespace Excel
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ExcelApplication app = new ExcelApplication())
            {
                try
                {
                    app.TransformAllFiles();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception occured: " + ex.Message);
                }
                finally
                {
                    Process[] pProcesses = Process.GetProcessesByName("Excel");
                    foreach (Process process in pProcesses)
                    {
                        process.Kill();
                    }

                    Thread.Sleep(10000);
                }
            }
        }
    }
}
